# 5 min PostgreSQL Analysis

## Scope

![PostgreSQL LOGO](postgressql.png) This is an example project that let you analyze and review the code of **PostgreSQL 16.3** in 5 min or less.

## Requirements

- [ ] Have Docker installed on your machine (only if you want to do the analysis yourself)
- [ ] Have Git installed on your machine
- [ ] Have a license key for SciTools Understand with DevOps Enabled (Later, we will say the key is (LICENSE_KEY))
- [ ] We assume the host is Windows, but feel free to adjust some command to match on Mac or Linux

**Have Questions?** : Contact US.Sales@Emenda.com

## 2 Options: Do it or Not!

### Option 1: Only open the results

<details><summary>Get the results on Windows...</summary>

```
git clone https://gitlab.com/EmendaUSA/demos/5-min-postgressql-analysis.git
cd .\5-min-postgressql-analysis\results
.\OpenInUnderstand.bat
```

**After the first open, you can simply reopen the project in Understand. No need to use the script**

</details>

<details><summary>Get the results on Linux or Mac...</summary>

```
git clone https://gitlab.com/EmendaUSA/demos/5-min-postgressql-analysis.git
cd ./5-min-postgressql-analysis/results
./OpenInUnderstand.bash
```

**After the first open, you can simply reopen the project in Understand. No need to use the script**

</details>


### Option 2: Create it yourself, go to the next paragraph!

## You are the doing it! Great! Ready, Set, Go!

- On the **HOST** (your machine), run:

```
git clone https://gitlab.com/EmendaUSA/scitools-devops
cd scitools-devops\Docker Images\scitools-runner
.\download-scitools.cmd
.\download-scitools-essential.cmd
.\build
docker network create "ci-network"
.\run_bash_container.cmd
```
If all goes well, you are now at the prompt of a bash shell with a prompt looking like `scitools_user@24e09d56fa3c:~$`

- In the **CONTAINER** (at the prompt), run:

```
sudo apt-get install build-essential libreadline-dev libicu-dev libreadline-devzlib1g-dev flex bison libxml2-dev libxslt-dev libssl-dev libxml2-utils xsltproc ccache pkg-config
```
Password is "pass"
```
git clone https://github.com/postgres/postgres.git
cd postgres
git checkout REL_16_3
./configure
bear -- make
```
Make sure to replace (LICENSE_KEY) by your license key!
```
und -setlicensecode (LICENSE_KEY)
```

```
create_database_advanced.bash -db scitools.und -cpp -gcc gcc
remove_symlinks.bash -dir . -pattern "*.[ch]*"
und add -cmake ./compile_commands.json -analyzelater -onetime -db scitools.und 
und analyze -db scitools.und
zip_database_advanced.bash -db scitools.und/ -zip scitools.zip
und -deregisterlicensecode
```
**Do not exit or kill the container just yet !!!**

Select a (DIRECTORY) on your host.

- On the **HOST** (your machine), _**modify**_ and run:
```
docker cp scitools-runner-testing:/home/scitools_user/postgres/scitools.zip (DIRECTORY)
```

- In the **CONTAINER** (at the prompt), run:
```
exit
```
This will close the container

- Finally, On the **HOST** (your machine), _**modify**_ and run:
```
und -setlicensecode (LICENSE_KEY)
cd (DIRECTORY)
tar.exe -xf .\scitools.zip
und settings -C++AddFoundSystemFiles off -db .\scitools.und
und add -root DEPENDENCIES_USR=".\usr" -db .\scitools.und
und analyze -db .\scitools.und
understand .\scitools.und
```
This will open Understand with PostgresSQL analyzed!

**IMPORTANT: You don't need to run this every time you open the project, just run `understand .\scitools.und` or use the GUI**

Enjoy!

## I get 116 errors, is that normal?

Well, yes and no. 

Understand C++ Engine is build on top of CLANG, and the code is GCC Code. In this specific example, all the errors are in /usr/lib/gcc/x86_64-linux-gnu/ folder, **Not in the PostgresSQL code itself**. All those errors relates to built-in x86_64 instructions that conflict between CLANG and GCC. 

We can fix it by hiding the problematic definition "deep" in the gcc header files. To do so, simply use the [fix.ini](./fix.ini) file using:
```
und settings -C++MacrosAdd @".\fix.ini" -db .\scitools.und
```

There is only one in .\src\common\config_info.c, "expected expression". That one is due to an error parsing the compile_commands.json for macro VAL_CFLAGS for file `.\src\common\config_info.c`... UPDATE: This will not happen after build 1188 :)

To fix it, simply run:
```
und settings -override_c++_macrosAdd ".\src\common\config_info.c" VAL_CFLAGS="\"\"" -db .\scitools.und
```

**Conclusion: You good to go! Should be 0 error now.**

## I want to do this on another project. Help me!

It's really simple, you only have to change a few lines...

Replace ```sudo apt-get install build-essential libreadline-dev zlib1g-dev flex bison libxml2-dev libxslt-dev libssl-dev libxml2-utils xsltproc ccache pkg-config``` by the package you need.

Replace 
```
git clone https://github.com/postgres/postgres.git
cd postgres
git checkout REL_16_3
./configure
bear -- make
```
by:
- your project `git clone`
- your directory `cd`
- your Tag `git checkout`
- Replace `./configure` by your pre-build actions
- Replace `make` in `bear -- make` by your build command.

**There are options to work with any build OS, any Cross Compilation or any tool chain. Please contact US.Sales@Emenda.com**
